drop table if exists sk_user;

/*==============================================================*/
/* Table: sk_user                                               */
/*==============================================================*/
create table sk_user
(
   user_id              bigint not null auto_increment ,
   nick_name            varchar(20)  ,
   login_name           varchar(20) ,
   avatar_url           varchar(255),
   status               tinyint default 1,
   role                 tinyint default 1 ,
   phone                varchar(11)  ,
   email                varchar(255)  ,
   passwd               varchar(100),
   device_num           varchar(20) ,
   register_time        datetime default CURRENT_TIMESTAMP ,
   last_login_time      datetime ,
   other_login_id       bigint ,
   address              varchar(255) ,
   is_del               tinyint default 0 ,
   primary key (user_id)
);

