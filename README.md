#study-rabbitMq-spring
####此工程是学习 RabbitMq，Spring，MyBatis整合的一个工程，仅供参考。

##一，配置文件说明
 `applicationContext-consumer.xml` 关于链接工厂的配置文件，主要配置链接信息，初始化listener类
 `applicationContext-publisher.xml` 关于链接工厂的配置文件，主要配置链接信息，初始化publisher类
 `rabbitMq-consumer-spring.xml` 关于consumer的配置信息，队列的声明，交换机的声明，监听器的配置等
 `rabbitMq-publisher-spring.xml`关于publisher的配置信息队列的声明，交换机的声明，监听器的配置等
 `spring-datasource.xml` 配置数据源信息
`spring-mybatis.xml` myBatis的配置信息
 `global.properties`  配置数据库链接信息，rabbitMq的链接地址
##二，已经有功能
#### 1，publisher两种方式实现
&emsp;&emsp;a,与spring整合实现（`RunPublisher.java`）
&emsp;&emsp;b. 基本于jar实现(`RunPublisherTwo.java`)
#### 2，一个交换机两个队列每个队列对应10个消费者
&emsp;&emsp;请查看 `RunPublisher.java`,`rabbitMq-publisher-spring.xml` `rabbitMq-consumer-spring.xml`
&emsp;&emsp;要运行 `RunPublisher.java` ,`RunConsumer.java`即可进行测试
#### 3，消费者对数据库操作
&emsp;&emsp;请运行`RunConsumer.java`,`RunPublisherThree.java`进行测试
