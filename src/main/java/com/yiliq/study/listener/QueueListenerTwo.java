package com.yiliq.study.listener;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

import java.io.UnsupportedEncodingException;

/**
 * Created by hasee on 2016/5/13.
 */
public class QueueListenerTwo implements MessageListener {
    public void onMessage(Message message) {
        try {
            System.out.println("["+QueueListenerTwo.class.getName()+"]["+Thread.currentThread().getName()+"] 收到的消息："+new String(message.getBody(),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
