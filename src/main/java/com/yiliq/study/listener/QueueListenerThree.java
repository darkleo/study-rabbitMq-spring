package com.yiliq.study.listener;

import com.alibaba.fastjson.JSONObject;
import com.yiliq.study.model.SkUser;
import com.yiliq.study.service.SkUserService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Created by hasee on 2016/5/14.
 */
@Service("queueListenerThree")
public class QueueListenerThree implements MessageListener {

    @Autowired
    @Qualifier("skUserService")
    public SkUserService skUserService;


    public void onMessage(Message message) {
        try {
            String msg= new String(message.getBody(),"utf-8");
            System.out.println("[QueueListenerThree] recive message '"+msg+"'");
            JSONObject jsonObject = JSONObject.parseObject(msg);
            String deviceId = jsonObject.getString("deviceId");
            Integer type = jsonObject.getInteger("type");
            SkUser dbSkUser = skUserService.queryBydeviceNum(deviceId);
            System.out.println("[QueueListenerThree] user info 'phone:"+dbSkUser.getPhone()+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
