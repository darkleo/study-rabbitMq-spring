package com.yiliq.study.mapper;

import java.util.List;

/**
 * 标记型接口，使用MapperScanfConfig扫描使用
 * @author darkleo
 * @date Mar 2, 2016 1:51:19 PM
 *	查询方法定义：
 *	带参数的查询（不分页）使用query*
 *	带参数的查询（分页）使用find*
 */
public interface SuperMapper<PK,TYPE> {
	/**
	 * 保存数据
	 * @param entity
	 * @return 操作行数
	 */
	Integer save(TYPE entity);
	/**
	 * 根据数据来id来查询数据
	 * @param id 数据id
	 * @return java对象
	 */
	TYPE queryById(PK id);
	/**
	 * 查询所有
	 * @return
	 */
	List<TYPE> queryAll();
	/**
	 * 根据实体的id来进行数据的修改
	 * @param entity
	 * @return 操作的行数 
	 */
	Integer updateById(TYPE entity);
	/**
	 * 根据实体的id来删除数据
	 * @param id 数据id
	 * @return 操作的行数
	 */
	Integer deleteById(PK id);
}
