package com.yiliq.study.mapper;


import com.yiliq.study.model.SkUser;

public interface SkUserMapper extends SuperMapper<Long, SkUser> {

	SkUser queryByMobile(String mobile);

	SkUser queryBydeviceNum(String deviceId);
}