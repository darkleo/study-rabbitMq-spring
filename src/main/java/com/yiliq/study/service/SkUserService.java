package com.yiliq.study.service;


import com.yiliq.study.model.SkUser;

public interface SkUserService {
	/**
	 * 根据手机号来查询数据
	 * @param mobile 手机号
	 * @return
	 */
	SkUser queryByMobile(String mobile);
	/**
	 * 根据设备号来查询数据
	 * @param deviceId 设备id
	 * @return
	 */
	SkUser queryBydeviceNum(String deviceId);
}
