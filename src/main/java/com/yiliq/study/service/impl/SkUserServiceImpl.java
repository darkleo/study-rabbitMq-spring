package com.yiliq.study.service.impl;

import com.yiliq.study.mapper.SkUserMapper;
import com.yiliq.study.mapper.SuperMapper;
import com.yiliq.study.model.SkUser;
import com.yiliq.study.service.SkUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("skUserService")
public class SkUserServiceImpl implements SkUserService {
	
	@Autowired
	public SkUserMapper skUserMapper;
	
	public SuperMapper<Long, SkUser> getMapper() {
		return this.skUserMapper;
	}

	public SkUser queryByMobile(String mobile) {
		return skUserMapper.queryByMobile(mobile);
	}

	public SkUser queryBydeviceNum(String deviceId) {
		return skUserMapper.queryBydeviceNum(deviceId);
	}
}
