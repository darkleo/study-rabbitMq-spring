package com.yiliq.study.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by hasee on 2016/5/13.
 */
public class RunConsumer {

    public static void main(String[] args){
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/rabbitmq/applicationContext-consumer.xml");
    }
}
