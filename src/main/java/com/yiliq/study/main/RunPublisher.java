package com.yiliq.study.main;

import com.yiliq.study.publisher.DevicePublisher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hasee on 2016/5/13.
 */
public class RunPublisher {
    public static ApplicationContext applicationContextPulishuer  = null;
    static {
        applicationContextPulishuer = new ClassPathXmlApplicationContext("config/rabbitmq/applicationContext-publisher.xml");
    }
    public static void main(String[] args) throws InterruptedException {
        doublePublisher();
    }
    private static  void doublePublisher() throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
            executorService.execute(new Runnable() {
                public void run() {
                    try {
                        signlePublisher(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        executorService.execute(new Runnable() {
            public void run() {
                try {
                    signlePublisher(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private static void signlePublisher(int x) throws InterruptedException {
        DevicePublisher devicePublisher  = (DevicePublisher) applicationContextPulishuer.getBean("devicePublish");
        String message = null;
        int count = 0;
        String queueKey = x==1?"test_queue_key_one":"test_queue_key_two";
        while(true){

            message= "["+queueKey+"]["+Thread.currentThread().getName()+"]---client["+ ++count +"]:Hello rabbitMq spring!";
            devicePublisher.sendDataToQueue(queueKey,message);
            System.out.println(message);
            Thread.sleep(20L);
        }
    }
    private static void creatThread(){

    }
}
