package com.yiliq.study.publisher;

/**
 * Created by hasee on 2016/5/13.
 */
public interface Publisher {
    void sendDataToQueue(String queueKey,Object data);
}
