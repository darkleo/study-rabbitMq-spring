package com.yiliq.study.publisher;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by hasee on 2016/5/13.
 * 设备发部者
 */
public class DevicePublisher implements Publisher{

    @Autowired
    private AmqpTemplate amqpTemplate;


    public void sendDataToQueue(String queueKey, Object data) {
        try{
            amqpTemplate.convertAndSend(queueKey,data);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
